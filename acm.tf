module "acm" {
  source  = "terraform-aws-modules/acm/aws"

  domain_name  = keys(module.zones.route53_zone_name)[0]
  zone_id      = keys(module.zones.route53_zone_zone_id)[0]

  wait_for_validation = true

  tags = local.tags
}