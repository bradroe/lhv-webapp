module "zones" {
  source  = "terraform-aws-modules/route53/aws//modules/zones"

  zones = {
    "lhv-example.com" = {
      comment = "route53 zone for webapp"
    }
  }

  tags = local.tags
}

resource "aws_route53_record" "webapp" {
  zone_id = keys(module.zones.route53_zone_zone_id)[0]
  name    = "webapp"
  type    = "A"

  alias {
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
    evaluate_target_health = true

  }

  depends_on = [module.zones]
}