**Overview**
==========

This Terraform project creates a Webapp in AWS running on Elastic Container Service using FARGATE. An ALB is deployed infront of the webapp that only accepts traffic on HTTP and HTTPS, (HTTP is redirect to HTTPS) and is then forwarded to application on HTTP, SSL terminiation is handled in the load balancer layer. A certifiacte is created using amazon certificate manager and attached to the load balancer, this certificate would be trusted by all browsers by default.

Instead of using the default VPC a new VPC is created so subnets can be seperated into private and public and resources created in the relavent subnets. 

A route53 zone is created along with route53 records to create a friendly DNS name.

**Architecture**
==========
![Screenshot](Capture.PNG)
