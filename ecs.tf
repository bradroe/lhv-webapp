module "ecs" {
  source = "terraform-aws-modules/ecs/aws"

  name               = "lhv-ecs"
  container_insights = true
  capacity_providers = ["FARGATE", "FARGATE_SPOT"]
  default_capacity_provider_strategy = [
    {
      capacity_provider = "FARGATE_SPOT"
    }
  ]

  tags = local.tags
}

resource "aws_ecs_service" "ecs-service" {
  name            = "lhv-webapp"
  cluster         = module.ecs.ecs_cluster_id
  task_definition = aws_ecs_task_definition.ecs-service.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets         = module.aws_vpc.private_subnets
    security_groups = [module.ecs_security_group.security_group_id]
  }

  load_balancer {
    target_group_arn = module.alb.target_group_arns[0]
    container_name   = "lhv-webapp"
    container_port   = 80
  }

  tags = local.tags

}

resource "aws_ecs_task_definition" "ecs-service" {
  family                   = "lhv-webapp"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  container_definitions = jsonencode([
    {
      name      = "lhv-webapp"
      image     = "hello-world"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
    ]
  )
}

module "ecs_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "lhv-ecs-sg"
  description = "Security group for usage with ECS task"
  vpc_id      = module.aws_vpc.vpc_id

  ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = module.alb_security_group.security_group_id
    }
  ]

  egress_rules = ["all-all"]
}