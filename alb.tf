module "alb" {
  source = "terraform-aws-modules/alb/aws"

  name = "lhv-alb"

  load_balancer_type = "application"

  vpc_id          = module.aws_vpc.vpc_id
  subnets         = module.aws_vpc.public_subnets
  security_groups = [module.alb_security_group.security_group_id]

  target_groups = [
    {
      name_prefix      = "lhv-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = module.acm.acm_certificate_arn
      target_group_index = 0
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  tags = local.tags
}

module "alb_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "lhv-alb-sg"
  description = "Security group for usage with ALB"
  vpc_id      = module.aws_vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp"]
  egress_rules        = ["all-all"]
}